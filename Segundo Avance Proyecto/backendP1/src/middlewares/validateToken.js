import jwt from 'jsonwebtoken'
import { TOKEN_SECRET } from '../config.js'

export const authRequired = (req, res, next) => {
    const {cookie} = req.cookies;
    if (!cookie) {
        return res.status(401).json({ message: 'No autorizado' });
    }

    jwt.verify(cookie, TOKEN_SECRET, (err, user) => {
        if (err) {
            return res.status(401).json({ message: 'Invalid Token' });
        }
        req.user = user;
        next();
    })
}