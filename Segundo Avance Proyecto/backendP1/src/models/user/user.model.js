import mongoose from "mongoose";
import generateId from "../../services/userPre.js";

// Modelo para el usuario
const userSchema = new mongoose.Schema({
    id: { type: Number },
    nombre: { type: String, required: true},
    apellido: { type: String, required: true },
    email: { type: String, required: true, unique: true, trim: true },
    ubicacion: { type: String, default: null},
    password: { type: String, required: true },
    role: { type: String, default: 'User' }
}, {
    timestamps: true
});

generateId(userSchema);



export default mongoose.model('User', userSchema);
