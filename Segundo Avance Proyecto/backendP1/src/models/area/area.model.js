import mongoose from "mongoose";

const areaSchema = new mongoose.Schema({
    name: { type: String, required: true },
    director: { type: String, required: true }, // One user directs multiple areas
    codigo: [{ type: String}],
    dispositivos: [{ type: String}]
  });
  
export default mongoose.model('Area', areaSchema);
  