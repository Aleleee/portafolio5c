import mongoose from "mongoose";

const instalacionSchema = new mongoose.Schema({
    user: { type: String, required: true },
    location: { type: String, required: true },
    codigo: { type: String, required: true },
    asigna: { type: String, default: ''},
    instalador: { type: String, default: ''}
});

export default mongoose.model('Instalacion', instalacionSchema);
