import mongoose from 'mongoose';

const paqueteSchema = new mongoose.Schema({
  id: { type: String, unique: true },
  nombre: { type: String, required: true },
  descripcion: { type: String, required: false },
  costo: { type: Number, required: true },
  dispositivos: [{ type: String, required: true }],
});

// nota: Puede ser el almacen del usuario que crea el paqueta o el que se decida?

// Pre-save middleware to generate the custom id
paqueteSchema.pre('save', async function (next) {
  try {
    const paquete = this;

    if (paquete.isNew) {
      // Generate the custom id based on specific fields
      const idNombre = paquete.nombre.slice(0, 2).toUpperCase();
      let counter = await mongoose.model('Paquete').countDocuments({}).exec() + 1;
      paquete.id = `${idNombre}${counter}`;

      next(); // Call next() when everything is ready
    } else {
      next(); // Skip id generation for updates
    }
  } catch (error) {
    next(error); // Pass any error to next()
  }
});

export default mongoose.model('Paquete', paqueteSchema);
