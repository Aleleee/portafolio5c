import mongoose from "mongoose";

const stockSchema = new mongoose.Schema({
    almacen: {type: String, required: true},
    tipo: {type: String, required: true},
    disponible: {type: Number, default: 0}
})

export default mongoose.model('Stock', stockSchema)