import mongoose, { Schema } from "mongoose";

// Helper function to generate the code
function generateCodigo() {
  const letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let result = '';
  for (let i = 0; i < 5; i++) {
    result += letters.charAt(Math.floor(Math.random() * letters.length));
  }
  const digit = Math.floor(Math.random() * 10);
  return result + digit;
}

const ventaSchema = new mongoose.Schema({
  id: { type: Number, unique: true },
  fecha: { type: Date, default: Date.now },
  total: { type: Number, required: true },
  user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  paquete: [{ type: String, required: true }],
  codigo: { type: String, unique: true } // Make sure codigo is unique
});

ventaSchema.pre('save', async function (next) {
  const doc = this;

  if (doc.isNew) {
    try {
      // Generate unique ID
      const lastVenta = await mongoose.model('Venta').findOne().sort({ id: -1 }).exec();
      doc.id = lastVenta ? lastVenta.id + 1 : 1;

      // Generate unique codigo
      let uniqueCodigo;
      let isUnique = false;
      while (!isUnique) {
        uniqueCodigo = generateCodigo();
        const existingVenta = await mongoose.model('Venta').findOne({ codigo: uniqueCodigo });
        if (!existingVenta) {
          isUnique = true;
        }
      }
      doc.codigo = uniqueCodigo;

      next();
    } catch (error) {
      next(error);
    }
  } else {
    next();
  }
});

export default mongoose.model('Venta', ventaSchema);
