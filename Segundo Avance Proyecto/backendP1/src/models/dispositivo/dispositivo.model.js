import mongoose from "mongoose";

// Definir el esquema base
const dispositivoSchemaBase = new mongoose.Schema({
  id: { type: String },
  almacen: { type: String},
  modelo: {type: String, required: true},
  cantidad: { type: Number}
});

dispositivoSchemaBase.pre('save', async function (next) {
  try {
    const dispositivo = this;
    let counter = await mongoose.model('Dispositivo').countDocuments({}).exec() + 1;
    dispositivo.id = counter;

    next(); // Llamar a next() cuando todo esté listo
  } catch (error) {
    next(error); // Pasar cualquier error a next()
  }
});

dispositivoSchemaBase.pre('insertMany', async function (next, docs) {
  try {
    const count = await mongoose.model('Dispositivo').countDocuments({}).exec();
    for (let i = 0; i < docs.length; i++) {
      docs[i].id = count + i + 1;
    }
    next();
  } catch (error) {
    next(error); // Pasar cualquier error a next()
  }
});

const Dispositivo = mongoose.model("Dispositivo", dispositivoSchemaBase);

const dispositivoTemperaturaHumedadSchema = new mongoose.Schema({
  tamano: { type: Number, default: 2},
  temperatura: { type: Number, default: null },
  humedad: { type: Number, default: null }
});

const dispositivoLuzSchema = new mongoose.Schema({
  tamano: { type: Number, default: 1},
  intensidadLuz: { type: Number, default: null }
});

const DispositivoTemperaturaHumedad = Dispositivo.discriminator('DispositivoTemperaturaHumedad', dispositivoTemperaturaHumedadSchema);
const DispositivoLuz = Dispositivo.discriminator('DispositivoLuz', dispositivoLuzSchema);

export { Dispositivo, DispositivoTemperaturaHumedad, DispositivoLuz };
