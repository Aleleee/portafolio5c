import mongoose from "mongoose";

export const dispositivoTipoSchema = new mongoose.Schema({
    nombre: { type: String, required: true },
    modelo: { type: String, required: true },
    tipo: { type: String, required: true }
})

export default mongoose.model('DispositivoTipo', dispositivoTipoSchema);