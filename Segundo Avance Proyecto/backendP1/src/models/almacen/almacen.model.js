import mongoose from "mongoose";

const almacenSchema = new mongoose.Schema({
    id: { type: Number},
    ubicacion: { type: String, required: true },
    nombre: { type: String, required: true },
    capacidad: { type: Number, required: true },
    encargado: { type: String, required: true },
    disponible: { type: Number, default: 0 },
  });

  almacenSchema.pre('save', async function (next) {
    try {
        const almacen = this;
        if (almacen.isNew) {
            const lastAlmacen = await mongoose.model('Almacen').findOne({}, {}, { sort: { 'id': -1 } });
            const newId = lastAlmacen ? lastAlmacen.id + 1 : 1;
            almacen.id = newId;
        }

        next();
    } catch (error) {
        next(error);
    }
});
  
export default mongoose.model('Almacen', almacenSchema);
  