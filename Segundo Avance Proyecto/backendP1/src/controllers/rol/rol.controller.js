import Role from '../../models/rol/rol.model.js'

export const createRol = async (req, res) => {
    const { tipo } = req.body;
    try {
        if (!tipo) {
            return res.status(400).json({ message: 'Se requiere el campo tipo' });
        }
        const newRol = new Role({ tipo });
        await newRol.save();

        res.status(201).json(newRol);
    } catch (error) {
        console.error('Error al crear rol:', error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};


export const getRoles = async(req, res) => {
    try {
        const roles = await Role.find();
        res.status(200).json(roles);
    } catch (error) {
        console.log(error);
    }
}

export const getRol = async (req, res) => {
    const { id } = req.params; 
    try {
        const rol = await Role.findOne({ tipo: id });
        if (!rol) {
            return res.status(404).json({ message: 'Rol no encontrado' });
        }
        res.status(200).json(rol);
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};


export const updateRol = async (req, res) => {
    const { id } = req.params;
    const { tipo } = req.body; 
    try {
        if (!campo) {
            return res.status(400).json({ message: 'Se requiere el campo id' });
        }

        if (!tipo) {
            return res.status(400).json({ message: 'Se requiere el campo tipo' });
        }

        const rolActualizado = await Role.findOneAndUpdate(
            { tipo: id },
            { tipo: tipo }, 
            { new: true } 
        );

        if (!rolActualizado) {
            return res.status(404).json({ message: 'Rol no encontrado' });
        }

        res.status(200).json(rolActualizado);
    } catch (error) {
        console.error('Error al actualizar el rol:', error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};


export const deleteteRol = async(req, res) => {
    const { id } = req.params;
    try {
        const rol = await Role.findOneAndDelete({tipo: id})
        res.status(200).json(rol);
    } catch (error) {
        console.log(error);
    }
}