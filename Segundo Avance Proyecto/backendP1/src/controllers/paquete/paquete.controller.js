import Paquete from "../../models/paquete/paquete.model.js";
import { Dispositivo } from "../../models/dispositivo/dispositivo.model.js";
import Stock from "../../models/stock/stock.model.js";

export const createPaquete = async (req, res) => {
    const { nombre, descripcion, costo, dispositivos, modelos } = req.body;

    try {
        if (!costo || !dispositivos || !Array.isArray(dispositivos) || dispositivos.length === 0) {
            return res.status(400).json({ message: 'Se requiere el campo costo y dispositivos (como array con al menos un dispositivo)' });
        }

        const dispStock = await Stock.find({ tipo: { $in: modelos}})
        if (dispStock.length === 0 || dispStock.some(stock => stock.disponible === 0)) {
            return res.status(400).json({ message: 'Algunos modelos de dispositivo no existen en el stock o no hay disponibles por el momento' });
        }

        // Buscar los dispositivos por sus ids personalizados
        const foundDispositivos = await Dispositivo.find({ id: { $in: dispositivos } });
        
        if (foundDispositivos.length !== dispositivos.length) {
            return res.status(400).json({ message: 'Algunos dispositivos no existen' });
        }
        const existingPaquetes = await Paquete.find({ dispositivos: { $in: dispositivos } });

        if (existingPaquetes.length > 0) {
            return res.status(400).json({ message: 'Algunos dispositivos ya están en uso en otro paquete' });
        }
        


        // Crear el paquete con los ids personalizados de los dispositivos encontrados
        const newPaquete = new Paquete({
            nombre,
            descripcion,
            costo,
            dispositivos, // Guardar los ids personalizados directamente
        });

        await newPaquete.save();

        res.status(201).json(newPaquete);
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error al crear el paquete' });
    }
};

export const getPaquetes = async(req, res) => {
    try {
        const paquetes = await Paquete.find();
        res.status(200).json(paquetes);
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
}

export const getPaquete = async(req, res) => {
    const { id } = req.params;
    try {
        const paquete = await Paquete.findOne({ id: id });
        res.status(200).json(paquete);
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
}


export const updatePaquete = async (req, res) => {
    const { id } = req.params;
    const { nombre, descripcion, costo, dispositivos } = req.body;

    try {
        let updateData = { nombre, descripcion, costo };

        if (dispositivos) {
            const foundDispositivos = await Dispositivo.find({ id: { $in: dispositivos } });

            if (foundDispositivos.length !== dispositivos.length) {
                return res.status(400).json({ message: 'Algunos dispositivos no existen' });
            }

            updateData.dispositivos = foundDispositivos.map(d => d.id);
        }

        const existsPaquete = await Paquete.findOneAndUpdate({ id: id }, updateData, { new: true });

        if (!existsPaquete) {
            return res.status(404).json({ message: 'Paquete no encontrado' });
        }

        res.status(200).json(existsPaquete);
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error al actualizar el paquete' });
    }
};

export const deletePaquete = async (req, res) => {
    const { id } = req.params;
    try {
        const paquete = await Paquete.findOneAndDelete({ id: id });
        if (!paquete) {
            return res.status(404).json({ message: 'Paquete no encontrado' });
        }
        res.status(200).json({ message: 'Paquete eliminado correctamente' });
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};