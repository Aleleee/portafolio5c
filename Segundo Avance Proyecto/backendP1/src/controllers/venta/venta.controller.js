import Venta from "../../models/venta/venta.model.js";
import User from "../../models/user/user.model.js";
import Paquete from "../../models/paquete/paquete.model.js";

export const createVenta = async (req, res) => {
    const { paquete } = req.body;
    console.log(req.user);
    // Verificar que los campos obligatorios estén presentes
    if (!paquete || !Array.isArray(paquete) || paquete.length === 0) {
        return res.status(400).json({ message: 'Se requieren el usuario y al menos un paquete' });
    }

    try {
        // Verificar si el usuario existe
        const existUser = await User.findById(req.user.id);
        console.log(existUser);
        if (!existUser) {
            return res.status(404).json({ message: 'Usuario no encontrado' });
        }

        // Verificar si todos los paquetes existen y obtener los costos
        const existPaq = await Paquete.find({ id: { $in: paquete } });
        console.log(existPaq);
        if (existPaq.length !== paquete.length) {
            return res.status(404).json({ message: 'Uno o más paquetes no encontrados' });
        }

        // Calcular el total de los paquetes
        const total = existPaq.reduce((sum, pkg) => sum + pkg.costo, 0);
        console.log(total);

        for (const pkg of existPaq) {
            for (const dispositivoId of pkg.dispositivos) {
                const stockItem = await Stock.findOne({ tipo: dispositivoId });

                if (!stockItem || stockItem.disponible <= 0) {
                    return res.status(400).json({ message: `Stock insuficiente para el dispositivo con ID: ${dispositivoId}` });
                }

                stockItem.disponible -= 1;
                await stockItem.save();
            }
        }

        // Crear la nueva venta con el total calculado
        const newVenta = new Venta({ user: req.user.id, paquete, total });

        // Guardar la venta
        await newVenta.save();

        res.status(201).json(newVenta);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};



export const getVentas = async (req, res) => {
    try {
        const ventas = await Venta.find();
        res.status(200).json(ventas);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const getVenta = async(req, res) => {
    const { id } = req.params;

    try {
        const venta = await Venta.findOne({ id: id });
        res.status(200).json(venta);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const updateVenta = async (req, res) => {
    const { id } = req.params;
    const { user, paquete } = req.body;

    try {
        // Verificar si la venta existe
        const venta = await Venta.findOne({ id: id });
        if (!venta) {
            return res.status(404).json({ message: 'Venta no encontrada' });
        }

        // Verificar si el usuario existe (si se proporciona un nuevo usuario)
        if (user) {
            const existUser = await User.findOne({ id: user });
            if (!existUser) {
                return res.status(404).json({ message: 'Usuario no encontrado' });
            }
            venta.user = user;
        }

        // Verificar y actualizar los paquetes (si se proporciona un nuevo conjunto de paquetes)
        if (paquete) {
            if (!Array.isArray(paquete) || paquete.length === 0) {
                return res.status(400).json({ message: 'Se requieren al menos un paquete' });
            }

            const existPaq = await Paquete.find({ id: { $in: paquete } });
            if (existPaq.length !== paquete.length) {
                return res.status(404).json({ message: 'Uno o más paquetes no encontrados' });
            }

            venta.paquete = paquete;
        }

        // Guardar la venta actualizada
        await venta.save();

        res.status(200).json(venta);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

export const deleteVenta = async (req, res) => {
    const { id } = req.params;

    try {
        const venta = await Venta.findOneAndDelete({ id: id });

        if (!venta) {
            return res.status(404).json({ message: 'Venta no encontrada' });
        }

        res.status(200).json({ message: 'Venta eliminada exitosamente' });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

