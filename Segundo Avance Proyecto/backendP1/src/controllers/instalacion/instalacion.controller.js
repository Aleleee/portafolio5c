import Instalacion from "../../models/instalacion/instalacion.model.js";
import User from "../../models/user/user.model.js";

export const getInstalaciones = async(req, res) => {
    try {
        const instalaciones = await Instalacion.find();
        res.status(200).json(instalaciones);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const getInstalacion = async(req, res) => {
    const { id } = req.params;
    try {
        const instalacion = await Instalacion.findOne({ _id: id });
        res.status(200).json(instalacion);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const createInstalacion = async(req, res) => {
    const { user, location, codigo, asigna, instalador } = req.body;
    try {
        const newInstalacion = new Instalacion({
            user,
            location,
            codigo
        });
        await newInstalacion.save();
        res.status(200).json(newInstalacion);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const updateInstalacion = async (req, res) => {
    const { id } = req.params;
    const { asigna, location, instalador } = req.body;

    try {
        // Validar que los usuarios existen
        const asignaUser = await User.findOne({id:asigna});
        if (!asignaUser) {
            return res.status(404).json({ message: "Usuario asignado no encontrado" });
        }

        const instaladorUser = await User.findOne({id:instalador});
        if (!instaladorUser) {
            return res.status(404).json({ message: "Instalador no encontrado" });
        }

        const updatedInstalacion = await Instalacion.findOneAndUpdate(
            { _id: id },
            { asigna, location, instalador },
            { new: true, runValidators: true }
        );

        if (!updatedInstalacion) {
            return res.status(404).json({ message: "Instalación no encontrada" });
        }

        res.status(200).json(updatedInstalacion);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const deleteInstalacion = async(req, res) => {
    const { id } = req.params;
    try {
        const instalacion = await Instalacion.findOneAndDelete({ _id: id });
        res.status(200).json(instalacion);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}