import Almacen from '../../models/almacen/almacen.model.js'
import User from '../../models/user/user.model.js';

export const createAlmacen = async (req, res) => {
    const { ubicacion, nombre, capacidad, user } = req.body;
    try {
        const usuario = await User.findOne({id:user});
        if (!usuario) {
            return res.status(400).json({ message: 'El usuario especificado no existe' });
        }
        if (!ubicacion || !nombre || !capacidad || !user) {
            return res.status(400).json({ message: 'Se requieren todos los campos' });
        }
        const newAlmacen = new Almacen({ ubicacion, nombre, capacidad, user });
        await newAlmacen.save();

        res.status(201).json(newAlmacen);
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};

export const getAlmacenes = async (req, res) => {
    try {
        const almacenes = await Almacen.find();
        res.status(200).json(almacenes);
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
}

export const getAlmacen = async (req, res) => {
    const { id } = req.params;
    try {
        const almacen = await Almacen.findOne({ id: id });
        res.status(200).json(almacen);
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
}


export const updateAlmacen = async (req, res) => {
    const { id } = req.params;
    const updateFields = req.body;

    try {
        const almacen = await Almacen.findOneAndUpdate({ id: id }, updateFields, { new: true });

        if (!almacen) {
            return res.status(404).json({ message: 'Almacén no encontrado' });
        }

        res.status(200).json(almacen);
    } catch (error) {
        console.error('Error al actualizar almacén:', error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};

export const deleteAlmacen = async (req, res) => {
    const { id } = req.params;

    try {
        const almacen = await Almacen.findOneAndDelete(id);
        if (!almacen) {
            return res.status(404).json({ message: 'El almacén no existe' });
        }

        res.status(200).json({ message: 'Almacén eliminado correctamente' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};
