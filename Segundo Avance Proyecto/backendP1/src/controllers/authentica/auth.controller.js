import bcrypt from 'bcryptjs';
import User from '../../models/user/user.model.js';
import { createAccessToken } from '../../services/jwt.js';

export const Register = async (req, res) => {
    const { nombre, apellido, email, password, ubicacion} = req.body;

    try {
        if(!nombre || !apellido || !email || !password){
            return res.status(400).json({ message: 'Se requiere el nombre, apellido, email, password y role'});
        }
        
        const nameRegex = /^[a-zA-Z\s]+$/;
        if (!nameRegex.test(nombre)) {
            return res.status(400).json({ message: 'El nombre solo puede contener letras' });
        }

        if (!nameRegex.test(apellido)) {
            return res.status(400).json({ message: 'El apellido solo puede contener letras' });
        }

        const existingUser = await User.findOne({ email });
        if (existingUser) {
            return res.status(400).json({ message: 'El correo electrónico ya está en uso' });
        }

        const passwordHash = await bcrypt.hashSync(password, 10);
        const newUser = new User({
            nombre,
            apellido,
            email,
            password: passwordHash,
            ubicacion
        });

        const userSaved = await newUser.save();
        const token = await createAccessToken({id: userSaved._id, role: userSaved.role})
        res.cookie('cookie', token)
        res.status(201).json({
            id: userSaved._id,
            nombre: userSaved.nombre,
            apellido: userSaved.apellido,
            email: userSaved.email
        });
    } catch (error) {
        console.error('Error al crear usuario:', error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};

export const Login = async (req, res) => {
    const { email, password} = req.body;

    try {
        if(!email || !password){
            return res.status(400).json({ message: 'Se requiere el nombre, apellido, email, password y role'});
        }
        const existingUser = await User.findOne({ email });
        if (!existingUser) {
            return res.status(400).json({ message: 'El correo que ingreso no existe' });
        }

        const isMAtch = await bcrypt.compare(password, existingUser.password);
        if(!isMAtch) return res.status(400).json({ message: 'incorrect password'})

        const token = await createAccessToken({id: existingUser._id, role: existingUser.role, ubicacion: existingUser.ubicacion })
        res.cookie('cookie', token)
        res.status(201).json({
            id: existingUser._id,
            nombre: existingUser.nombre,
            apellido: existingUser.apellido,
            email: existingUser.email
        });
    } catch (error) {
        console.error('Error al crear usuario:', error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};

export const Logout = async (req, res) => {
    res.cookie('cookie', "", {
        expires: new Date(0)
    });
    res.status(200).json({ message: 'Logout correcto' });
}

export const profile = async (req, res) => {
    console.log(req.user);
    const userFound = await User.findById(req.user.id)

    if(!userFound) return res.status(400).json({ message: 'user not found'})

    return res.json({
        id: userFound._id,
        nombre: userFound.nombre,
        apellido: userFound.apellido,
        email: userFound.email,
        role: userFound.role
    })
}