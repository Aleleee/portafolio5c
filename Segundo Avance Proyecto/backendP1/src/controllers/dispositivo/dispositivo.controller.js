import { Dispositivo, DispositivoLuz, DispositivoTemperaturaHumedad } from "../../models/dispositivo/dispositivo.model.js";
import Almacen from "../../models/almacen/almacen.model.js";
import DispositivoTipo from "../../models/dispositivo/dispositivo_tipo.model.js";
import Stock from "../../models/stock/stock.model.js"

export const createDispositivo = async (req, res) => {
  const { modelo, cantidad } = req.body;
  try {
    const existsTipo = await DispositivoTipo.findOne({ modelo: modelo });
    const existsAlmacen = await Almacen.findOne({nombre: req.user.ubicacion});

    if (!existsAlmacen) {
      return res.status(400).json({ message: 'El almacen especificado no existe' });
    }

    if (!existsTipo) {
      return res.status(400).json({ message: 'El tipo de dispositivo especificado no existe' });
    }

    let tamanoTotal = 0;
    for (let i = 0; i < cantidad; i++) {
      let newDispositivo;
      if (existsTipo.tipo === 'DispositivoTemperaturaHumedad') {
        newDispositivo = new DispositivoTemperaturaHumedad({ modelo, almacen: existsAlmacen.nombre });
      } else if (existsTipo.tipo === 'DispositivoLuz') {
        newDispositivo = new DispositivoLuz({ modelo, almacen: existsAlmacen.nombre });
      } else {
        newDispositivo = new Dispositivo({ modelo, almacen: existsAlmacen.nombre });
      }
      tamanoTotal += newDispositivo.tamano;
    }

    const dispositivosEnAlmacen = await Dispositivo.find({ almacen: existsAlmacen.nombre });
    let tamanoActual = dispositivosEnAlmacen.reduce((total, dispositivo) => total + dispositivo.tamano, 0);

    if (tamanoActual + tamanoTotal > existsAlmacen.capacidad) {
      return res.status(400).json({ message: 'No hay suficiente capacidad en el almacén para estos dispositivos' });
    }

    const dispositivos = [];
    for (let i = 0; i < cantidad; i++) {
      let newDispositivo;
      if (existsTipo.tipo === 'DispositivoTemperaturaHumedad') {
        newDispositivo = new DispositivoTemperaturaHumedad({ modelo, almacen: existsAlmacen.nombre });
      } else if (existsTipo.tipo === 'DispositivoLuz') {
        newDispositivo = new DispositivoLuz({ modelo, almacen: existsAlmacen.nombre });
      } else {
        newDispositivo = new Dispositivo({ modelo, almacen: existsAlmacen.nombre });
      }
      dispositivos.push(newDispositivo);
    }

    await Dispositivo.insertMany(dispositivos);

    // Actualizar o crear stock
    const stock = await Stock.findOne({ almacen: existsAlmacen.nombre, tipo: modelo });
    if (stock) {
      stock.disponible += cantidad;
      await stock.save();
    } else {
      const newStock = new Stock({
        almacen: existsAlmacen.nombre,
        tipo: modelo,
        disponible: cantidad
      });
      await newStock.save();
    }

    res.status(201).json({ message: `${cantidad} dispositivos creados exitosamente` });
  } catch (error) {
    console.error('Error al crear dispositivos:', error);
    res.status(500).json({ message: 'Error interno del servidor' });
  }
};




export const getDispositivos = async (req, res) => {
  try {
    const dispositivos = await Dispositivo.find();
    res.status(200).json(dispositivos);
  } catch (error) {
    console.error('Error al obtener dispositivos:', error);
    res.status(500).json({ message: 'Error interno del servidor' });
  }
}

export const getDispositivo = async (req, res) => {
  const { id } = req.params;
  try {
    const dispositivo = await Dispositivo.findOne({id:id});
    res.status(200).json(dispositivo);
  } catch (error) {
    console.error('Error al obtener dispositivo:', error);
    res.status(500).json({ message: 'Error interno del servidor' });
  }
}

export const updateDispositivo = async (req, res) => {
  const { id } = req.params;
  const updateFields = req.body;

  try {
      const dispositivo = await Dispositivo.findOneAndUpdate({ id: id }, updateFields, { new: true });

      if (!dispositivo) {
            return res.status(404).json({ message: 'Dispositivo no encontrado' });
      }

        res.status(200).json(dispositivo);
  } catch (error) {
      console.error('Error al actualizar dispositivo', error);
      res.status(500).json({ message: 'Error interno del servidor' });
  }
};


export const deleteDispositivo = async (req, res) => {
  const { id } = req.params;
  try {
    const dispositivo = await Dispositivo.findOneAndDelete({ id: id });
    res.status(200).json(dispositivo);
  } catch (error) {
    console.error('Error al eliminar dispositivo:', error);
    res.status(500).json({ message: 'Error interno del servidor' });
  }
}