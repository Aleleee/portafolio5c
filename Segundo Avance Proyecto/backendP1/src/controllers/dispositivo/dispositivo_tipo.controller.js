import DispositivoTipo from "../../models/dispositivo/dispositivo_tipo.model.js";

export const createDispositivoTipo = async (req, res) => {
    const { tipo, nombre, modelo } = req.body;
    try {
        if (!tipo || !nombre || !modelo) {
            return res.status(400).json({ message: 'Se requiere el campo tipo' });
        }
        // Validar que no exista un dispositivo con el mismo modelo en el mismo tipo
        const existingDispositivoTipo = await DispositivoTipo.findOne({ modelo });
        if (existingDispositivoTipo) {
            return res.status(400).json({ message: 'Ya existe un dispositivo con ese modelo' });
        }

        const newDispositivoTipo = new DispositivoTipo({ tipo, nombre, modelo });
        await newDispositivoTipo.save();

        res.status(201).json(newDispositivoTipo);
    } catch (error) {
        console.log(error);
    }
}

export const getDispositivoTipos = async (req, res) => {
    try {
        const dispositivoTipos = await DispositivoTipo.find();
        res.status(200).json(dispositivoTipos);
    } catch (error) {
        console.log(error);
    }
}

export const getDispositivoTipo = async (req, res) => {
    const { tipo, nombre, modelo, _id } = req.body;

    // Construir la consulta dinámica
    let query = {};
    if (tipo) query.tipo = tipo;
    if (nombre) query.nombre = nombre;
    if (modelo) query.modelo = modelo;
    if (_id) query._id = mongoose.Types.ObjectId(_id); // Asegúrate de convertir el _id a ObjectId

    try {
        if (Object.keys(query).length === 0) {
            return res.status(400).json({ message: 'Se requiere al menos un campo de búsqueda (tipo, nombre, modelo, _id)' });
        }

        const dispositivos = await DispositivoTipo.find(query);
        res.status(200).json(dispositivos);
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};

export const updateDispositivoTipo = async (req, res) => {
    const { id } = req.params;
    const { tipo, nombre} = req.body;
    try {


        if (!id) {
            return res.status(400).json({ message: 'Se requiere el id'});
        }
        const dispositivoTipo = await DispositivoTipo.findOneAndUpdate({ modelo: id }, {nombre: nombre, tipo: tipo}, {new: true});
        if (!dispositivoTipo) {
            return res.status(404).json({ message: 'Dispositivo tipo no encontrado' });
        }

        res.status(200).json(dispositivoTipo);
    } catch (error) {
        console.log(error);
    }
}

export const deleteDispositivoTipo = async (req, res) => {
    const { id } = req.params;
    try {
        if (!id) {
            return res.status(400).json({ message: 'Se requiere el campo tipo' });
        }
        const dispositivoTipo = await DispositivoTipo.findOneAndDelete({ modelo: id });
        if (!dispositivoTipo) {
            return res.status(404).json({ message: 'Dispositivo tipo no encontrado' });
        }
        res.status(200).json(dispositivoTipo);
    } catch (error) {
        console.log(error);
    }
}