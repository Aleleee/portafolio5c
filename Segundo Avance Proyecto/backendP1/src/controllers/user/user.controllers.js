import bcrypt from 'bcryptjs';
import User from '../../models/user/user.model.js';
import Role from '../../models/rol/rol.model.js';
import { createAccessToken } from '../../services/jwt.js';

export const createUser = async (req, res) => {
    const { nombre, apellido, email, password, role, ubicacion} = req.body;

    try {
        if(!nombre || !apellido || !email || !password|| !role){
            return res.status(400).json({ message: 'Se requiere el nombre, apellido, email, password y role'});
        }
        const nameRegex = /^[a-zA-Z\s]+$/;

        if (!nameRegex.test(nombre)) {
            return res.status(400).json({ message: 'El nombre solo puede contener letras' });
        }

        if (!nameRegex.test(apellido)) {
            return res.status(400).json({ message: 'El apellido solo puede contener letras' });
        }
        
        const existingUser = await User.findOne({ email });
        if (existingUser) {
            return res.status(400).json({ message: 'El correo electrónico ya está en uso' });
        }
        const rol = await Role.findOne({ tipo: role });
        if (!rol) {
            return res.status(400).json({ message: 'El rol especificado no existe'});
        }

        const passwordHash = await bcrypt.hashSync(password, 10);
        const newUser = new User({
            nombre,
            apellido,
            email,
            password: passwordHash,
            role: rol.tipo,
            ubicacion
        });

        const userSaved = await newUser.save();
        const token = await createAccessToken({id: userSaved.id})
        res.cookie('cookie', token)
        res.status(201).json({
            id: userSaved.id,
            nombre: userSaved.nombre,
            apellido: userSaved.apellido,
            email: userSaved.email
        });
    } catch (error) {
        console.error('Error al crear usuario:', error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};

export const getUsers = async (req, res) => {
    try {
        const users = await User.find();
        res.status(200).json(users); 
    } catch (error) {
        console.error('Error al obtener usuarios:', error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};

export const getUser = async (req, res) => {
    const { id } = req.params;
    try {
        const user = await User.findOne({id : id});
        res.status(200).json(user);
    } catch (error) {
        console.error('Error al obtener usuario:', error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};

export const updateUser = async (req, res) => {
    const { id } = req.params;
    const updateFields = req.body;

    try {
        const user = await User.findOneAndUpdate({ id }, updateFields, { new: true });
        if (!user) {
            return res.status(404).json({ message: 'Usuario no encontrado' });
        }
        res.status(200).json(user);
    } catch (error) {
        console.error('Error al actualizar usuario:', error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};

export const deleteUser = async (req, res) => {
    const { id } = req.params;
    try {
        const user = await User.findOneAndDelete({ id });
        if (!user) {
            return res.status(404).json({ message: 'Usuario no encontrado' });
        }
        res.status(200).json({ message: 'Usuario eliminado correctamente' });
    } catch (error) {
        console.error('Error al eliminar usuario:', error);
        res.status(500).json({ message: 'Error interno del servidor' });
    }
};
