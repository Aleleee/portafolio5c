import Suscription from "../../models/suscription/suscription.model.js";
import User from "../../models/user/user.model.js";
import Venta from "../../models/venta/venta.model.js";
import Instalacion from "../../models/instalacion/instalacion.model.js";

export const createSuscription = async(req, res) => {
    const {location, codigo, instalacion } = req.body;

    // Verificar que los campos obligatorios estén presentes
    if (!location ||!codigo) {
        return res.status(400).json({ message: 'Se requieren todos los campos' });
    }

    const existUser = await User.findById(req.user._id)
    if (!existUser) {
        return res.status(404).json({ message: 'Usuario no encontrado' });
    }
    const existCodigo = await Venta.findOne({ codigo: codigo})
    if (!existCodigo) {
        return res.status(404).json({ message: 'Venta no encontrada' });
    }

    const newSuscription = new Suscription({
        user: existUser._id,
        instalacion: instalacion,
        location: location,
        codigo: codigo
    })

    await newSuscription.save()

    if (instalacion === true) {
        const newInstalacion = new Instalacion({
            user: existUser.username,
            location: location,
            codigo: codigo,
        });

        await newInstalacion.save();
    }

    res.status(201).json(newSuscription);

}

export const getSuscripciones = async(req, res) => {
    try {
        const suscripciones = await Suscription.find();
        res.status(200).json(suscripciones);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const getSuscripcion = async(req, res) => {
    const { id } = req.params;

    try {
        const suscripcion = await Suscription.findOne({ id: id });
        res.status(200).json(suscripcion);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const updateSuscripcion = async (req, res) => {
    const { id } = req.params;
    const { user, location, codigo, instalacion, activo } = req.body;

    try {
        // Verificar si la suscripción existe
        const suscripcion = await Suscription.findOne({id: id});
        if (!suscripcion) {
            return res.status(404).json({ message: 'Suscripción no encontrada' });
        }

        // Verificar si el usuario existe
        const existUser = await User.findOne({ id: user });
        if (!existUser) {
            return res.status(404).json({ message: 'Usuario no encontrado' });
        }

        // Verificar si el código de venta existe
        const existCodigo = await Venta.findOne({ codigo: codigo });
        if (!existCodigo) {
            return res.status(404).json({ message: 'Venta no encontrada' });
        }

        // Actualizar la suscripción
        const updatedSuscripcion = await Suscription.findOneAndUpdate(
            { id: id },
            {
                user: existUser.username,
                location,
                codigo,
                instalacion,
                activo
            },
            { new: true }
        );

        // Manejar la lógica de instalación
        if (instalacion === true) {
            const newInstalacion = new Instalacion({
                user: existUser.username,
                location,
                codigo,
            });
            await newInstalacion.save();
        }

        res.status(200).json(updatedSuscripcion);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

export const deleteSuscripcion = async (req, res) => {
    const { id } = req.params;

    try {
        // Verificar si la suscripción existe y eliminarla
        const suscripcion = await Suscription.findOne({id: id});
        if (!suscripcion) {
            return res.status(404).json({ message: 'Suscripción no encontrada' });
        }

        res.status(200).json({ message: 'Suscripción eliminada correctamente' });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};
