import express from 'express'
import morgan from 'morgan';
import cors from 'cors'
import rolRoutes from './routes/rol/rol.routes.js'
import userRoutes from './routes/user/user.routes.js'
import almacenRoutes from './routes/almacen/almacen.routes.js'
import dispositivoRoutes from './routes/dispositivo/dispositivo.routes.js'
import dispositivoTipoRoutes from './routes/dispositivo/dispositivo_tipo.routes.js'
import paqueteRoutes from './routes/paquete/paquete.routes.js'
import ventaRoutes from './routes/venta/venta.routes.js'
import suscripcionRoutes from './routes/suscription/suscription.routes.js'
import instalacionRoutes from './routes/instalacion/instalacion.routes.js'
import areaRoutes from './routes/area/area.routes.js'
import authRoutes from './routes/authtenti/auth.routes.js'
import cookieParser from 'cookie-parser'
import { authRequired } from './middlewares/validateToken.js';
import { isAdmin } from './middlewares/validateRol.js';


const app = express()

app.use(cors())
app.use(morgan('dev'))
app.use(express.json())
app.use(cookieParser())
app.use('/api/paquete', paqueteRoutes)
app.use('/auth', authRoutes)

app.use(authRequired)
app.use('/api/area', areaRoutes)
app.use('/api/instalacion', instalacionRoutes)
app.use('/api/suscripcion', suscripcionRoutes)
app.use('/api/venta', ventaRoutes)

app.use('/api/almacen', almacenRoutes)
app.use('/api/dispositivo', dispositivoRoutes)

app.use(isAdmin)
app.use('/api/dispositivo', dispositivoTipoRoutes)
app.use('/api/rol', rolRoutes)
app.use('/api/user', userRoutes)

export default app;