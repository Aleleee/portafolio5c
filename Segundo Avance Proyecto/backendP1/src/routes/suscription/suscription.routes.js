import { getSuscripciones, getSuscripcion, createSuscription, updateSuscripcion, deleteSuscripcion } from "../../controllers/suscripcion/suscripcion.controller.js";
import { Router } from "express";
import { authRequired } from "../../middlewares/validateToken.js";

const router = Router();

router.get("/suscripciones", getSuscripciones)
router.get("/suscripcion/:id", getSuscripcion)
router.post("/suscripcion", authRequired, createSuscription)
router.patch("/suscripcion/:id", updateSuscripcion)
router.delete("/suscripcion/:id", deleteSuscripcion)

export default router;