import { getVentas, getVenta, createVenta, updateVenta, deleteVenta } from '../../controllers/venta/venta.controller.js'
import { Router } from 'express'
import { authRequired } from '../../middlewares/validateToken.js'

const router = Router()

router.get('/ventas', getVentas)
router.get('/venta/:id', getVenta)
router.post('/venta', authRequired, createVenta)
router.patch('/venta/:id', updateVenta)
router.delete('/venta/:id', deleteVenta)

export default router;