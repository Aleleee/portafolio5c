import { Router } from "express";
import { createUser, getUsers, getUser, updateUser, deleteUser } from "../../controllers/user/user.controllers.js";

const router = Router();

router.get("/users", getUsers)
router.get("/user/:id", getUser)
router.post("/user", createUser)
router.patch("/user/:id", updateUser)
router.delete("/user/:id", deleteUser)

export default router;