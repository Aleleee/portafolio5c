import { Router } from "express";
import { createRol, getRoles, getRol, updateRol, deleteteRol } from "../../controllers/rol/rol.controller.js";

const router = Router();


router.get("/roles", getRoles)
router.get("/role/:id", getRol)
router.post("/role", createRol)
router.patch("/role/:id", updateRol)
router.delete("/role/:id", deleteteRol)

export default router;