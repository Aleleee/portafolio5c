import { getAreas, getArea, createArea, deleteArea, updateArea } from "../../controllers/area/area.controller.js";
import { Router } from "express";
import { authRequired } from '../../middlewares/validateToken.js'

const router = Router();

router.get("/areas", getAreas)
router.get("/area/:id", getArea)
router.post("/area", authRequired, createArea)
router.patch('/area/:id', updateArea)
router.delete("/area/:id", deleteArea)

export default router;