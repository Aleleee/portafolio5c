import { Register, Login, Logout, profile } from "../../controllers/authentica/auth.controller.js";
import { Router } from "express";
import { authRequired } from "../../middlewares/validateToken.js";


const router = Router();    


router.post("/register", Register)
router.post("/login", Login)
router.post("/logout", Logout)
router.get("/profile", authRequired, profile)

export default router;