import { Router } from "express";
import { createDispositivoTipo, getDispositivoTipos, getDispositivoTipo, updateDispositivoTipo, deleteDispositivoTipo } from "../../controllers/dispositivo/dispositivo_tipo.controller.js";

const router = Router();

router.get("/dispositivoTipos", getDispositivoTipos)
router.get("/dispositivoTipo/:id", getDispositivoTipo)
router.post("/dispositivoTipo", createDispositivoTipo)
router.patch("/dispositivoTipo/:id", updateDispositivoTipo)
router.delete("/dispositivoTipo/:id", deleteDispositivoTipo)

export default router;