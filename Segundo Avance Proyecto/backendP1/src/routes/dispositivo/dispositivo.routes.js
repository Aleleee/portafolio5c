import { Router } from "express";
import { createDispositivo, getDispositivos, getDispositivo, updateDispositivo, deleteDispositivo } from "../../controllers/dispositivo/dispositivo.controller.js";

const router = Router();


router.get("/dispositivos", getDispositivos)
router.get("/dispositivo/:id", getDispositivo)
router.post("/dispositivo", createDispositivo)
router.patch("/dispositivo/:id", updateDispositivo)
router.delete("/dispositivo/:id", deleteDispositivo)

export default router;