import { Router } from "express";
import { createAlmacen, getAlmacenes, getAlmacen, updateAlmacen, deleteAlmacen } from "../../controllers/almacen/almacen.controller.js";
import { authRequired } from "../../middlewares/validateToken.js";
import { isAdmin } from "../../middlewares/validateRol.js";

const router = Router();

router.get("/almacenes", authRequired, getAlmacenes)
router.get("/almacen/:id", authRequired, getAlmacen)
router.post("/almacen",authRequired, isAdmin, createAlmacen)
router.patch("/almacen/:id", authRequired, isAdmin, updateAlmacen)
router.delete("/almacen/:id", authRequired, isAdmin, deleteAlmacen)

export default router;