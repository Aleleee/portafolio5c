import { Router } from "express";
import { createPaquete, getPaquetes, getPaquete, updatePaquete, deletePaquete } from "../../controllers/paquete/paquete.controller.js";
import { authRequired } from "../../middlewares/validateToken.js";
import { isAdmin, isAlmacenista } from "../../middlewares/validateRol.js";

const router = Router()

router.get('/paquetes', getPaquetes )
router.get('/paquete/:id', getPaquete)
router.post('/paquete', authRequired, isAdmin, createPaquete)
router.patch('/paquete/:id', authRequired, isAdmin, isAlmacenista, updatePaquete)
router.delete('/paquete/:id',  authRequired, isAdmin, isAlmacenista, deletePaquete)

export default router;