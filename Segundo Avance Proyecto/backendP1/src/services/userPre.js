import mongoose from "mongoose";

const generateId = function (schema) {
    schema.pre('save', async function (next) {
        try {
            const doc = this;
            if (doc.isNew) {
                const lastDoc = await mongoose.model(doc.constructor.modelName).findOne({}, {}, { sort: { 'id': -1 } });
                const newId = lastDoc ? lastDoc.id + 1 : 1;
                doc.id = newId;
            }
            next();
        } catch (error) {
            next(error);
        }
    });
};

export default generateId;
